if exists("g:loaded_vimtask")
    finish
endif
let g:loaded_vimtask = 1

let s:save_cpo = &cpo
set cpo&vim

function! s:Hoge()
    echo "HOGEHOGE"
endfunction

command! -nargs=0 Hoge call s:Hoge()

let &cpo = s:save_cpo
unlet s:save_cpo

